const fs = require('fs')

const data = fs.readFileSync('./input', {encoding: 'utf-8'})
const items = data.split('\n')
const totals = []
let elvesProcessed = 0
let currAccum = 0
for (const curr of items) {
  const strats = [
    {
      matcher: v => v.length === 0,
      action: () => {
        elvesProcessed += 1
        console.log(`Processing elf ${elvesProcessed}`)
        totals.push(currAccum)
        currAccum = 0
      },
    }, {
      matcher: v => v.length > 0, // FIXME should check is number too
      action: v => {
        currAccum += parseInt(v)
      },
    }
  ]
  const strat = strats.find(s => s.matcher(curr))
  if (!strat) {
    throw new Error(`Programmer error: could not find strat for value '${curr}'`)
  }
  strat.action(curr)
}
totals.sort().reverse() // yeah, it's cheating
const most1 = totals[0]
console.log(`Top 1: ${most1}`)
const most2 = totals[1]
const most3 = totals[2]
console.log(`Top 3: ${most1}, ${most2}, ${most3}`)
const top3Total = most1 + most2 + most3
console.log(`Total: ${top3Total}`)
