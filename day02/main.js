const fs = require('fs')

// A, X = rock
// B, Y = paper
// C, Z = scissors

const scores = {
  rock: 1,
  paper: 2,
  scissors: 3,
  loss: 0,
  draw: 3,
  win: 6,
}

const plays = {
  opRock: 'A',
  opPaper: 'B',
  opScissors: 'C',
  myRock: 'X',
  myPaper: 'Y',
  myScissors: 'Z',
}

const data = fs.readFileSync('./input', {encoding: 'utf-8'})
const items = data.split('\n')
const totals = []
let gamesProcessed = 0
let totalScore = 0

const strats = {
  [`${plays.opRock} ${plays.myRock}`]: scores.rock + scores.draw,
  [`${plays.opPaper} ${plays.myPaper}`]: scores.paper + scores.draw,
  [`${plays.opScissors} ${plays.myScissors}`]: scores.scissors + scores.draw,
  [`${plays.opRock} ${plays.myScissors}`]: scores.scissors + scores.loss,
  [`${plays.opPaper} ${plays.myRock}`]: scores.rock + scores.loss,
  [`${plays.opScissors} ${plays.myPaper}`]: scores.paper + scores.loss,
  [`${plays.opRock} ${plays.myPaper}`]: scores.paper + scores.win,
  [`${plays.opPaper} ${plays.myScissors}`]: scores.scissors + scores.win,
  [`${plays.opScissors} ${plays.myRock}`]: scores.rock + scores.win,
}

for (const curr of items) {
  if (!curr) {
    break
  }
  const strat = strats[curr]
  if (!strat) {
    throw new Error(`No strat found for ${curr}`)
  }
  gamesProcessed += 1
  // console.log(`Game ${gamesProcessed}`)
  totalScore += strat
}
console.log(`Total score: ${totalScore}`)
